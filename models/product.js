const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    hsn: {
      type: String,
      required: true,
      unique: true,
    }, 
    description: {
      type: String,
      unique: true,
      required: true,
    },
    price: [{
      quantity: mongoose.Schema.Types.ObjectId,
    }]
  },
  {
    timestamps: true,
  }
);

const Product = mongoose.model('Product', productSchema);

module.exports = Product;
