const mongoose = require("mongoose");

const quotaSchema = new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    company: {
        type: String,
        required: true
    }, 
    date: {
        type: Date,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    quotation: [{
        product: mongoose.Schema.Types.ObjectId,
        price: mongoose.Schema.Types.ObjectId
    }],
    sellingPrice: {
        type: Number,
        required: true
    },
    file: {
        type: String,
        required: false
    }
})

const Quotation = mongoose.model('Quotation', quotaSchema)

module.exports = Quotation;