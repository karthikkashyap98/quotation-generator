const express = require('express');
const path = require('path');
const hbs = require('hbs');

const app = express();

require("./db/mongodb");


const publicDirectoryPath = path.join(__dirname, './public');
const viewsPath = path.join(__dirname, './templates/views');
const partialsPath = path.join(__dirname, './templates/partials');

app.set('env development')
app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);


app.use(express.json());
app.use(express.static(publicDirectoryPath));

// app.use("/api/createProducts", require(""))


app.get('', (req, res) => {
  res.render('index', {
    title: 'Triton Ventures',
    name: 'Karthik Kashyap',
  });
});

app.get('/createProduct', (req, res) => {
  res.render('createProduct', {
    title: 'Triton Ventures', 
    name: 'Karthik Kashyap',
  })
})

app.get('/products', (req, res) => {
  res.render('products', {
    title: 'Triton Ventures', 
    name: 'Karthik Kashyap',
  })
})

const productRouter = require('./api/product');
const quotationRouter = require('./api/quotation');

app.use(quotationRouter)
app.use(productRouter)


app.listen(3000, () => {
  console.log('Server is up on port 3000');
});
