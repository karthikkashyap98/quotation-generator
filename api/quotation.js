const Quotation = require('../models/quotation');
const express = require('express');
fs = require('fs');

const router = new express.Router();

const readWriteAsync = require('../utils/fileEdit')

router.post('/quotation', async(req, res) => {
    console.log(req.body)
    const quotation = new Quotation({ ...req.body })
    try {
        await quotation.save();
        res.status(201).send(quotation);
      } catch (e) {
        res.status(400).send(e);
      }
});

router.get('/quotation', async(req, res) => {
    if (!req.query.search) {
        return res.send({
          error: "You must provide search term",
        });
      }
      try {
        quotation = await Quotation.findOne({ company: { $regex: req.query.search, $options: 'i' } });
        res.send({quotation})
      } catch (error) {
        return res.status(404).send({
            error,
          });
      }
});

router.get('/prepareQuotation', async(req, res) => {
    try {
      // await fs.writeFileSync('files/test.docx', 'hey')
      data = "Check this out"
      await readWriteAsync('files/test.docx', data)
      res.send("Done")
    } catch (error) {
      res.status(404).send(error)
    }
});

module.exports = router;