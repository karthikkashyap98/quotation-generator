const express = require("express")
const router = new express.Router();
const Product = require("../models/product")

router.post("/products", async (req, res) => {
    console.log(req.body)
    const product = new Product({ ...req.body })
    try {
        await product.save();
        res.status(201).send(product);
      } catch (e) {
        res.status(400).send(e);
      }
})

router.get("/products", async(req, res) => {
    if (!req.query.search) {
        return res.send({
          error: "You must provide search term",
        });
      }
      try {
        product = await Product.findOne({ name: { $regex: req.query.search, $options: 'i' } });
        res.send({product})
      } catch (error) {
        return res.status(404).send({
            error,
          });
      }
})

module.exports = router;