const fs = require("fs")
const docx = require("docx");

async function readWriteAsync(file, text) {
    const doc = new docx.Document();
    console.log("here")
    doc.addSection({
        properties: {},
        children: [
            new Paragraph({
                children: [
                    new TextRun("Hello World"),
                    new TextRun({
                        text: "Foo Bar",
                        bold: true,
                    }),
                    new TextRun({
                        text: "\tGithub is the best",
                        bold: true,
                    }),
                ],
            }),
        ],
    });
    console.log("here")
    // Used to export the file into a .docx file
    docx.Packer.toBuffer(doc).then((buffer) => {
        fs.writeFileSync("My Document.docx", buffer);
    });

    fs.readFile(file, 'utf-8', function(err, data){
      if (err) throw err;
      console.log(typeof(data))
      data += text;
        
      fs.writeFile(file, data, 'utf-8', function (err) {
        if (err) throw err;
        console.log('filelistAsync complete');
      });
    });
  }

module.exports = readWriteAsync;